-- delete database if exists
DROP DATABASE IF EXISTS writerdb;

-- create database 'writerdb'
CREATE DATABASE writerdb;

-- connect to database
\c writerdb;


-- create table 'writer'
DROP TABLE IF EXISTS writer;   -- optional: delete the existed table
CREATE TABLE writer
(
  id    INT PRIMARY KEY NOT NULL,
  name  VARCHAR(30) NOT NULL UNIQUE,
  age   REAL
);

-- list tables
\d 

INSERT INTO writer VALUES
      (1, 'Rabindranath Tagore', 80),
      (2, 'Leo Tolstoy', 82);

-- insert without age, as it can be NULL
INSERT INTO writer (id, name) VALUES
      (3, 'Pearl Buck');

-- insert one more data
INSERT INTO writer (id, age, name) VALUES
      (4, 30, 'Meher Krishna Patel');



-- create table book with auto-increment i.e. 'serial'
CREATE TABLE book -- creating table 'book'
    (
     writer_id  INT NOT NULL,               
     book_id  SERIAL NOT NULL PRIMARY KEY,
     title CHAR(100) NOT NULL,                
     price INT         
    );
CREATE TABLE

