.. PostgreSQL Guide documentation master file, created by
   sphinx-quickstart on Wed Apr 25 19:17:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PostgreSQL Guide's documentation!
============================================

.. toctree::
   :numbered:
   :maxdepth: 2
   :caption: Contents:

   postgresql/overview

